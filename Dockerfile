FROM golang:1.13.1 AS builder

WORKDIR /go/src/app
COPY . .
RUN go build -o /volunteer-bot

FROM frolvlad/alpine-glibc:alpine-3.10

RUN apk --update --no-cache add ca-certificates

COPY --from=builder /volunteer-bot /usr/local/bin/volunteer

EXPOSE 8080
CMD [ "/usr/local/bin/volunteer" ]
