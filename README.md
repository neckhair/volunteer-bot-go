# Volunteer Bot

This is a Slack bot which alles to choose a volunteer for certain tasks.

```text
/volunteer Who wants to have a cookie?
```

This produces a message with a button in the channel. The first user in the channel who clicks the button
confirms the message. The confirmation is shown to all other users in the channel.

HTTP endpoints:

* `POST /slash`: Receives the slash command from Slack and posts the request into the channel.
* `POST /volunteer`: Receives the confirmation from Slack and posts the confirmation message into the channel.
* `GET /_health`: Just returns "OK". It is meant to be used as a health check for Openshift.

## Installation

There will be a Docker image to run it easily. Until then, use the following command:

```sh
export SLACK_VERIFICATION_TOKEN=abcd
export GIN_MODE=release
volunteer
```

It will listen on port 8080.

## Development

Volunteer uses [Task](https://github.com/go-task/task) as build system. Install it like they tell you in their Readme.

```sh
export SLACK_VERIFICATION_TOKEN=abcd
task run
```

Slack needs to post data to your machine. Unless you have a public IP address you have to use
[ngrok](https://ngrok.com/) and configure your HTTP address in Slack.
