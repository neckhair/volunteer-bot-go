package bot

import (
	"encoding/json"
	"fmt"

	"gitlab.nine.ch/ninech/volunteer-bot/slack"
	"gitlab.nine.ch/ninech/volunteer-bot/stats"

	"github.com/gin-gonic/gin"
)

const messageColor = "#3498db"

// confirmVolunteerHandler receives the data from Slack and shows the
// name of the volunteer to the other users in the channel
func confirmVolunteerHandler(ctx *gin.Context) {
	payload := ctx.PostForm("payload")
	message := slack.Message{}
	err := json.Unmarshal([]byte(payload), &message)
	if err != nil {
		ctx.JSON(400, gin.H{"message": fmt.Sprintf("err parsing payload: %s", err.Error())})
		return
	}

	if len(message.OriginalMessage.Attachments) == 0 {
		ctx.JSON(400, gin.H{"message": fmt.Sprintf("invalid message")})
		return
	}

	stats.Increment(message.Team.ID, message.User.Name)

	response := gin.H{
		"response_type": "in_channel",
		"attachments": []gin.H{
			gin.H{
				"title": message.OriginalMessage.Attachments[0].Title,
				"text":  message.OriginalMessage.Attachments[0].Text,
				"color": messageColor,
			},
			gin.H{
				"text":  fmt.Sprintf(":white_check_mark: %s", randomThankYou(message.User.Name)),
				"color": messageColor,
			},
		},
	}

	ctx.JSON(200, response)
}
