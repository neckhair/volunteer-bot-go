package bot

import "github.com/gin-gonic/gin"

// SetupRouter starts a webserver to handle Slack's API requests
func SetupRouter() *gin.Engine {
	r := gin.Default()
	r.Use(gin.Recovery())

	r.POST("/slash", slashCommandHandler)
	r.POST("/volunteer", confirmVolunteerHandler)
	r.GET("/_health", func(ctx *gin.Context) {
		ctx.String(200, "OK")
	})

	return r
}
