package bot

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"gitlab.nine.ch/ninech/volunteer-bot/slack"
	"gitlab.nine.ch/ninech/volunteer-bot/stats"

	"github.com/bitly/go-simplejson"
	"github.com/stretchr/testify/assert"
)

func postSlashCommand(values *url.Values) (*http.Request, *httptest.ResponseRecorder) {
	router := SetupRouter()
	recorder := httptest.NewRecorder()

	req, _ := http.NewRequest("POST", "/slash", strings.NewReader(values.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	router.ServeHTTP(recorder, req)

	return req, recorder
}

func TestSlashCommandHandlerPostBack(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "POST", r.Method)
		assert.Equal(t, "/response", r.URL.EscapedPath())

		requestJSON, err := simplejson.NewFromReader(r.Body)
		assert.NoError(t, err)

		assert.Equal(t, "in_channel", requestJSON.Get("response_type").MustString())

		attachment := requestJSON.Get("attachments").MustArray()[0].(map[string]interface{})
		assert.Equal(t, "submitted text", attachment["text"])
		assert.Equal(t, "patrick is looking for a volunteer", attachment["title"])

		w.WriteHeader(200)
	}))
	defer ts.Close()

	slack.Config.VerificationToken = "abcd"
	values := &url.Values{}
	values.Add("token", slack.Config.VerificationToken)
	values.Add("user_name", "patrick")
	values.Add("text", "submitted text")
	values.Add("response_url", fmt.Sprintf("%s/response", ts.URL))

	_, recorder := postSlashCommand(values)
	assert.Equal(t, 200, recorder.Code)
}

func TestSlashCommandHandlerWeeklyStats(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "POST", r.Method)
		assert.Equal(t, "/response", r.URL.EscapedPath())

		requestJSON, err := simplejson.NewFromReader(r.Body)
		assert.NoError(t, err)

		assert.Equal(t, "in_channel", requestJSON.Get("response_type").MustString())

		attachment := requestJSON.Get("attachments").MustArray()[0].(map[string]interface{})
		assert.Equal(t, "bob: 3\nalice: 2", attachment["text"])
		assert.Equal(t, "Stats for this week", attachment["title"])

		w.WriteHeader(200)
	}))
	defer ts.Close()

	// Setup stats
	mockStore := &stats.MockStore{
		UserScores: stats.UserScores{
			stats.UserScore{Username: "bob", Score: 3},
			stats.UserScore{Username: "alice", Score: 2}}}

	stats.Store = mockStore

	// Setup command
	slack.Config.VerificationToken = "abcd"
	values := &url.Values{}
	values.Add("token", slack.Config.VerificationToken)
	values.Add("user_name", "patrick")
	values.Add("text", "stats")
	values.Add("response_url", fmt.Sprintf("%s/response", ts.URL))

	_, recorder := postSlashCommand(values)
	assert.Equal(t, 200, recorder.Code)
}

func TestSlashCommandHandlerWithInvalidToken(t *testing.T) {
	slack.Config.VerificationToken = "abcd"
	values := &url.Values{}
	values.Add("token", "ebcd")

	_, recorder := postSlashCommand(values)
	assert.Equal(t, 401, recorder.Code)
}
