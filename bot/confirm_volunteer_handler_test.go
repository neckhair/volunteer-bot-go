package bot

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"gitlab.nine.ch/ninech/volunteer-bot/slack"
	"gitlab.nine.ch/ninech/volunteer-bot/stats"

	"github.com/stretchr/testify/assert"
)

func postVolunteerRequest(payloadJSON string) (*http.Request, *httptest.ResponseRecorder) {
	router := SetupRouter()
	recorder := httptest.NewRecorder()

	values := url.Values{}
	values.Add("payload", payloadJSON)

	req, _ := http.NewRequest("POST", "/volunteer", strings.NewReader(values.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	router.ServeHTTP(recorder, req)

	return req, recorder
}

func TestConfirmVolunteerHandlerWithValidMessage(t *testing.T) {
	mockStore := &stats.MockStore{}
	stats.Store = mockStore

	payloadJSON := `{
		"user": {"id": "U1", "name": "paul"},
		"team": {"id": "T1", "domain": "ultra"},
		"original_message": {
			"attachments": [{"title": "eva is looking for a volunteer", "text": "Who wants to play?"}]
		}
	}`

	_, recorder := postVolunteerRequest(payloadJSON)

	assert.Equal(t, 200, recorder.Code)

	var response slack.Message
	json.Unmarshal([]byte(recorder.Body.String()), &response)

	assert.Equal(t, "in_channel", response.ResponseType)
	assert.Equal(t, "Who wants to play?", response.Attachments[0].Text)
	assert.Equal(t, "eva is looking for a volunteer", response.Attachments[0].Title)

	assert.Regexp(t, `^stats:T1:`, mockStore.IncrementedKey)
	assert.Equal(t, "paul", mockStore.IncrementedUsername)
}

func TestConfirmVolunteerHandlerWithInvalidJSON(t *testing.T) {
	_, recorder := postVolunteerRequest(`{`)

	assert.Equal(t, 400, recorder.Code)
}
func TestConfirmVolunteerHandlerWithInvalidMessage(t *testing.T) {
	_, recorder := postVolunteerRequest(`{}`)
	assert.Equal(t, 400, recorder.Code)
}
