package bot

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"gitlab.nine.ch/ninech/volunteer-bot/stats"

	"gitlab.nine.ch/ninech/volunteer-bot/slack"

	"github.com/gin-gonic/gin"
)

// slashCommandHandler parses the received data and creates a Slack message
// with a button to volunteer.
func slashCommandHandler(ctx *gin.Context) {
	s, err := slack.ParseSlashCommand(ctx.Request)
	if err != nil {
		ctx.JSON(400, gin.H{"message": "Could not parse the input data."})
		return
	}

	if !s.ValidateToken(slack.Config.VerificationToken) {
		ctx.JSON(401, gin.H{"message": "Invalid authentication token."})
		return
	}

	defer func() {
		response := buildResponse(s)
		jsonValue, _ := json.Marshal(response)
		_, err = http.Post(s.ResponseURL, "application/json", bytes.NewBuffer(jsonValue))
		if err != nil {
			log.Println(err)
		}
	}()

	ctx.String(200, "")
}

func buildResponse(s slack.SlashCommand) gin.H {
	switch {
	case strings.HasPrefix(s.Text, "stats"):
		return buildResponseForWeeklyStats(s)
	default:
		return buildResponseForVolunteerButton(s)
	}
}

func buildResponseForVolunteerButton(s slack.SlashCommand) gin.H {
	return gin.H{
		"response_type": "in_channel",
		"attachments": []gin.H{
			gin.H{
				"title":       fmt.Sprintf("%s is looking for a volunteer", s.UserName),
				"text":        s.Text,
				"callback_id": "choose_volunteer",
				"color":       "#3498db",
				"actions": []gin.H{
					gin.H{"name": "me", "text": "I volunteer!", "type": "button", "value": "me", "style": "primary"},
				},
			},
		},
	}
}

func buildResponseForWeeklyStats(s slack.SlashCommand) gin.H {
	userScores, _ := stats.GetWeekly(s.TeamID)
	userScores.Sort()

	var text []string
	for _, score := range userScores {
		text = append(text, score.Username+": "+strconv.Itoa(score.Score))
	}
	return gin.H{
		"response_type": "in_channel",
		"attachments": []gin.H{
			gin.H{
				"title": "Stats for this week",
				"text":  strings.Join(text, "\n"),
				"color": "#3498db",
			},
		},
	}
}
