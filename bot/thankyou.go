package bot

import (
	"fmt"
	"math/rand"
)

var thankYouMessages = []string{
	"Thank you for volunteering, %s!",
	"You are my sunshine, %s!",
	"We owe you something, %s. Thank you!",
	"Oh man, you're the hero of the day, %s!",
	"You again, %s? You're awesome!",
	"%s won the race. Thank you!",
}

// randomThankYou returns some random thank you massage which always includes a name.
func randomThankYou(name string) string {
	message := thankYouMessages[rand.Intn(len(thankYouMessages))]
	return fmt.Sprintf(message, name)
}
