package stats

import "sort"

// UserScore is a username, score pair
type UserScore struct {
	Username string
	Score    int
}

// UserScores is a list of user scores
type UserScores []UserScore

// Find looks for a username and returns its score
func (u UserScores) Find(username string) (int, *UserScore) {
	for idx, userScore := range u {
		if userScore.Username == username {
			return idx, &userScore
		}
	}
	return -1, &UserScore{}
}

// Sort sorts the user scores descending
func (u UserScores) Sort() {
	var scores sort.Interface = u
	sort.Sort(sort.Reverse(scores))
}

// ***
// Implementation of sort.Interface{}
// ***

// Len is the number of elements in the collection.
func (u UserScores) Len() int {
	return len(u)
}

// Less reports whether the element with
// index i should sort before the element with index j.
func (u UserScores) Less(i, j int) bool {
	return u[i].Score < u[j].Score
}

// Swap swaps the elements with indexes i and j.
func (u UserScores) Swap(i, j int) {
	u[i], u[j] = u[j], u[i]
}
