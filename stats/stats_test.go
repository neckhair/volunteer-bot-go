package stats

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInitialize(t *testing.T) {
	t.Run("when a REDIS_URL is set", func(t *testing.T) {
		valueBefore := os.Getenv("REDIS_URL")
		defer func() { os.Setenv("REDIS_URL", valueBefore) }()

		os.Setenv("REDIS_URL", "redis://localhost:1")
		err := Initialize()
		if err != nil {
			t.Fatal(err)
		}
		_, ok := Store.(*RedisStore)
		if !ok {
			t.Fatal("the store was not set correctly to a RedisStore")
		}
	})

	t.Run("when no REDIS_URL is set", func(t *testing.T) {
		valueBefore := os.Getenv("REDIS_URL")
		defer func() { os.Setenv("REDIS_URL", valueBefore) }()

		os.Unsetenv("REDIS_URL")

		err := Initialize()
		if err != nil {
			t.Fatal(err)
		}

		_, ok := Store.(*MemoryStore)
		if !ok {
			t.Fatalf("the store type should be MemoryStore")
		}
	})
}

func TestStatsIncrement(t *testing.T) {
	mockStore := &MockStore{}
	Store = mockStore

	Increment("T04A", "paul")

	assert.Regexp(t, `^stats:T04A:week:20\d{4}$`, mockStore.IncrementedKey)
	assert.Equal(t, "paul", mockStore.IncrementedUsername)
}

func TestStatsGetWeekly(t *testing.T) {
	userScore := UserScore{"paul", 2}
	mockStore := &MockStore{UserScores: UserScores{userScore}}
	Store = mockStore

	t.Run("when queried with a valid team id", func(t *testing.T) {
		userScores, err := GetWeekly("T04A")
		assert.Nil(t, err)
		assert.Equal(t, 2, userScores[0].Score)
	})
}
