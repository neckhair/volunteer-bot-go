package stats

// MockStore is just a mocked store for tests
type MockStore struct {
	IncrementedKey      string
	IncrementedUsername string
	UserScores          UserScores
}

// Increment just sets the passed values and returns 1
func (s *MockStore) Increment(key string, username string) (int, error) {
	s.IncrementedKey = key
	s.IncrementedUsername = username
	return 1, nil
}

// Get does nothing at the moment
func (s *MockStore) Get(key string) (UserScores, error) {
	return s.UserScores, nil
}
