package stats

import (
	"fmt"
	"testing"

	"github.com/alicebob/miniredis"
	"github.com/stretchr/testify/assert"
)

// make sure RedisStore implements the StatisticsStore interface
var redisStore StatisticsStore = &RedisStore{}

func TestNewRedisStore(t *testing.T) {
	t.Run("with a valid URL", func(t *testing.T) {
		redisStore, err := NewRedisStore("redis://localhost:1234/0")
		if err != nil {
			t.Fatal(err)
		}

		redisHost := redisStore.Client.Options().Addr
		if redisHost != "localhost:1234" {
			t.Errorf("redis host is %s, but should be localhost:1234", redisHost)
		}
	})

	t.Run("with an invalid URL", func(t *testing.T) {
		_, err := NewRedisStore("yolo123")
		if err == nil {
			t.Error("no error was returned")
		}
	})
}

func usingFakeRedis(f func(string)) {
	redisServer, err := miniredis.Run()
	if err != nil {
		panic(err)
	}
	defer redisServer.Close()

	redisURL := fmt.Sprintf("redis://%s", redisServer.Addr())

	f(redisURL)
}

func TestIncrement(t *testing.T) {
	t.Run("when it can connect to a redis instance", func(t *testing.T) {
		usingFakeRedis(func(redisURL string) {
			store, err := NewRedisStore(redisURL)
			assert.NoError(t, err)

			value, err := store.Increment("week1", "paul")
			assert.NoError(t, err)
			assert.Equal(t, 1, value)
		})
	})

	t.Run("when it cannot connect to redis", func(t *testing.T) {
		store, err := NewRedisStore("redis://somehost:9999")
		assert.NoError(t, err)

		value, err := store.Increment("week1", "paul")
		assert.Error(t, err)
		assert.Equal(t, 0, value)
	})
}

func TestGet(t *testing.T) {
	t.Run("when it can connect to a redis instance", func(t *testing.T) {
		usingFakeRedis(func(redisURL string) {
			store, err := NewRedisStore(redisURL)
			assert.NoError(t, err)

			store.Increment("week1", "paul")
			userScores, err := store.Get("week1")
			assert.NoError(t, err)
			assert.Equal(t, 1, userScores[0].Score)
		})
	})

	t.Run("when it cannot connect to redis", func(t *testing.T) {
		store, err := NewRedisStore("redis://localhost:1")
		assert.NoError(t, err)

		userScores, err := store.Get("week1")
		assert.Error(t, err)
		assert.Empty(t, 0, userScores)
	})
}
