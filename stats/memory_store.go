package stats

// MemoryStore is the default backend and stores everything in memory
type MemoryStore struct {
	Statistics map[string]UserScores
}

// Increment increments the score for the user
// if the value is nil it initializes it with zero and increments it
func (b *MemoryStore) Increment(key, username string) (int, error) {
	if b.Statistics == nil {
		b.Statistics = make(map[string]UserScores)
	}
	if b.Statistics[key] == nil {
		b.Statistics[key] = UserScores{}
	}

	idx, userScore := b.Statistics[key].Find(username)
	if idx >= 0 {
		b.Statistics[key][idx].Score++
	} else {
		userScore.Username = username
		userScore.Score = 1
		b.Statistics[key] = append(b.Statistics[key], *userScore)
	}
	return userScore.Score, nil
}

// Get returns the value at "key"
func (b *MemoryStore) Get(key string) (UserScores, error) {
	return b.Statistics[key], nil
}
