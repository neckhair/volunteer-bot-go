package stats

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMemoryStoreIncrement(t *testing.T) {
	backend := MemoryStore{}

	// Increment the first time
	value, err := backend.Increment("week1", "phil")
	assert.NoError(t, err)
	assert.Equal(t, 1, value)

	// Increment a second time
	backend.Increment("week1", "phil")
	userScores, _ := backend.Get("week1")
	assert.Equal(t, 2, userScores[0].Score)

	// Increment for another week
	value, err = backend.Increment("week2", "paul")
	assert.NoError(t, err)
	assert.Equal(t, 1, value)
}

func TestMemoryStoreGet(t *testing.T) {
	backend := MemoryStore{}

	userScores, err := backend.Get("key1")
	assert.NoError(t, err)
	assert.Empty(t, userScores)

	backend.Increment("key1", "paul")
	userScores, err = backend.Get("key1")
	assert.NoError(t, err)
	assert.Equal(t, "paul", userScores[0].Username)
	assert.Equal(t, 1, userScores[0].Score)
}
