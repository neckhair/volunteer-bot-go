package stats

import (
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUserScoresImplementsSortInterface(t *testing.T) {
	assert.Implements(t, (*sort.Interface)(nil), UserScores{})
}

func TestUserScoresFind(t *testing.T) {
	userScore := UserScore{Username: "paul", Score: 1}
	userScores := UserScores{userScore}

	t.Run("with an included username", func(t *testing.T) {
		index, foundUserScore := userScores.Find("paul")

		assert.Equal(t, 0, index)
		assert.Equal(t, &userScore, foundUserScore)
	})

	t.Run("when the username is not found", func(t *testing.T) {
		index, _ := userScores.Find("alice")

		assert.Equal(t, -1, index)
	})
}
