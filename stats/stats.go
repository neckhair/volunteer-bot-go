package stats

import (
	"fmt"
	"os"
	"time"
)

const weeklyKeyPattern string = "stats:%s:week:%d%02d"

// StatisticsStore is an interface for a key/value store
type StatisticsStore interface {
	Increment(key, username string) (int, error)
	Get(key string) (UserScores, error)
}

// Store is a key value store to save statistics
var Store StatisticsStore = &MemoryStore{}

// Initialize makes the store ready for connections
func Initialize() error {
	redisURL, ok := os.LookupEnv("REDIS_URL")
	if !ok {
		Store = &MemoryStore{}
		return nil
	}

	var err error
	Store, err = NewRedisStore(redisURL)
	return err
}

// Increment saves a score for a user
// It maintains scores for the current week (for the moment)
func Increment(teamID string, username string) error {
	_, err := Store.Increment(getCurrentWeekKey(teamID), username)
	return err
}

// GetWeekly returns this week's list of user scores
func GetWeekly(teamID string) (UserScores, error) {
	scores, err := Store.Get(getCurrentWeekKey(teamID))
	if err != nil {
		return UserScores{}, err
	}
	scores.Sort()
	return scores, nil
}

func getCurrentWeekKey(teamID string) string {
	year, week := time.Now().ISOWeek()
	return fmt.Sprintf(weeklyKeyPattern, teamID, year, week)
}
