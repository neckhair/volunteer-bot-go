package stats

import (
	"strconv"

	"github.com/go-redis/redis"
)

// RedisStore stores its stats in an instance of Redis
// There is a key for every week and team. It contains a Redis hash of { user => score }.
//
// Example session in the redis-cli:
//
// 127.0.0.1:6379> KEYS *
// 1) "stats:T04M4MVPQ:week:201812"
// 127.0.0.1:6379> HGET stats:T04M4MVPQ:week:201812 phil
// "1"
type RedisStore struct {
	Client *redis.Client
}

// NewRedisStore initializes a new redis store from the given url
func NewRedisStore(redisURL string) (*RedisStore, error) {
	options, err := redis.ParseURL(redisURL)
	if err != nil {
		return nil, err
	}
	return &RedisStore{Client: redis.NewClient(options)}, nil
}

// Increment increments the number at the given key by 1.
func (s *RedisStore) Increment(key, username string) (int, error) {
	result, err := s.Client.HIncrBy(key, username, 1).Result()
	return int(result), err
}

// Get returns the scores for the given key
func (s *RedisStore) Get(key string) (UserScores, error) {
	result, err := s.Client.HGetAll(key).Result()
	if err != nil {
		return UserScores{}, err
	}

	scores := UserScores{}
	for username, score := range result {
		value, _ := strconv.Atoi(score)
		userScore := UserScore{Username: username, Score: value}
		scores = append(scores, userScore)
	}
	return scores, nil
}
