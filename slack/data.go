package slack

import "github.com/nlopes/slack"

// User represents a Slack user
type User struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

// Team represents a Slack team
type Team struct {
	ID     string `json:"id"`
	Domain string `json:"domain"`
}

type Attachment struct {
	Text    string                   `json:"text"`
	Title   string                   `json:"title"`
	Actions []map[string]interface{} `json:"actions"`
}

// Message represents a message received from Slack
type Message struct {
	ResponseType    string        `json:"response_type"`
	Type            string        `json:"type"`
	CallbackID      string        `json:"callback_id"`
	User            User          `json:"user"`
	Team            Team          `json:"team"`
	OriginalMessage slack.Message `json:"original_message"`
	Attachments     []Attachment  `json:"attachments"`
}
