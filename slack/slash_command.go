package slack

import (
	"net/http"

	"github.com/nlopes/slack"
)

// SlashCommand represents data from a Slack slash command
type SlashCommand struct {
	slack.SlashCommand
}

// ParseSlashCommand parses a request from a Slack slash command
func ParseSlashCommand(r *http.Request) (s SlashCommand, err error) {
	parsedData, err := slack.SlashCommandParse(r)
	return SlashCommand{parsedData}, err
}
