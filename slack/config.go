package slack

import (
	"errors"
	"os"
)

// SlackConfig holds some configuration settings for Slack
type config struct {
	// When Slack sends a slash command we have to verify it with this token
	VerificationToken string

	// The OAuth token is used to connect to Slack's API, for example to get user information.
	OAuthToken string
}

// Config contains some configuration variables for Slack
var Config = &config{}

// Initialize gets the configuration from environment variables.
func (c *config) Initialize() error {
	c.VerificationToken = os.Getenv("SLACK_VERIFICATION_TOKEN")
	if c.VerificationToken == "" {
		return errors.New("please provide the environment variable SLACK_VERIFICATION_TOKEN")
	}

	c.OAuthToken = os.Getenv("SLACK_OAUTH_TOKEN")
	return nil
}
