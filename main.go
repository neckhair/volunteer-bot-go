package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"

	"gitlab.nine.ch/ninech/volunteer-bot/bot"
	"gitlab.nine.ch/ninech/volunteer-bot/slack"
	"gitlab.nine.ch/ninech/volunteer-bot/stats"
)

func main() {
	err := slack.Config.Initialize()
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	err = stats.Initialize()
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	rand.Seed(time.Now().UnixNano())

	r := bot.SetupRouter()
	err = r.Run()
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
