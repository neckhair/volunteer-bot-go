module gitlab.nine.ch/ninech/volunteer-bot

go 1.13

require (
	github.com/alicebob/gopher-json v0.0.0-20180125190556-5a6b3ba71ee6
	github.com/alicebob/miniredis v0.0.0-20180220160213-9bec9d56d714
	github.com/bitly/go-simplejson v0.5.0
	github.com/davecgh/go-spew v1.1.0
	github.com/garyburd/redigo v1.6.0
	github.com/gin-contrib/sse v0.0.0-20170109093832-22d885f9ecc7
	github.com/gin-gonic/gin v1.1.5-0.20170702092826-d459835d2b07
	github.com/go-redis/redis v6.10.2+incompatible
	github.com/golang/protobuf v1.0.0
	github.com/gorilla/websocket v1.2.0
	github.com/mattn/go-isatty v0.0.3
	github.com/nlopes/slack v0.2.1-0.20180307173413-91c91a11ad0b
	github.com/pmezard/go-difflib v1.0.0
	github.com/stretchr/testify v1.2.1
	github.com/ugorji/go v0.0.0-20180112141927-9831f2c3ac10
	github.com/yuin/gopher-lua v0.0.0-20180316054350-84ea3a3c79b3
	golang.org/x/sys v0.0.0-20180202135801-37707fdb30a5
	gopkg.in/go-playground/validator.v8 v8.18.2
	gopkg.in/yaml.v2 v2.0.0
)
